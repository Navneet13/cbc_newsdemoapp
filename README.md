# CBC_NewsDemoApp
 The code is based on Kotlin.

### Software Requirements -
  - IDE: [Android Studio]
 
### Project Setup
  - Extract/ Unzip the project
  - Import the project in Android Studio.
  - A dialog will pop-up to Trust the Project from outside source or stay in safe mode.
  - Project is build automatically using gradle.
    Alternatively, build the project by going to Menu Bar > Build > Rebuild Project
  - Run `app` configuration

### Architecture overview
 App has a single module:
 `app` - Contains all the app features of the app, views, activities

### External dependencies
  - [Retrofit](https://square.github.io/retrofit/) - For making the network calls
  - [Gson](https://github.com/google/gson) - Java Serialisation/ De-serialisation library
  - [Coil](https://github.com/coil-kt/coil) - Image Loading library backed by Kotlin Coroutines
  - [Mockito](https://github.com/mockito/mockito) - Mocking framework for Unit Testing

### Repository Link
  - [Bitbucket Link](https://bitbucket.org/Navneet13/cbc_newsdemoapp/src)