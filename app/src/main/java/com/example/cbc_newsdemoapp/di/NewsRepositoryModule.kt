package com.example.cbc_newsdemoapp.di

import com.example.cbc_newsdemoapp.repository.NewsRepository
import com.example.cbc_newsdemoapp.repository.NewsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)

abstract class NewsRepositoryModule {
    @Binds
    abstract fun providesNewsRepository(newsRepository: NewsRepositoryImpl): NewsRepository
}