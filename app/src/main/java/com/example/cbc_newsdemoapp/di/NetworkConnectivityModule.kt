package com.example.cbc_newsdemoapp.di

import android.content.Context
import com.example.cbc_newsdemoapp.utils.NetworkConnectivityHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkConnectivityModule {
    @Singleton
    @Provides
    fun providesNetworkConnectivityHelper(@ApplicationContext appContext: Context) : NetworkConnectivityHelper{
        return NetworkConnectivityHelper(appContext)
    }
}