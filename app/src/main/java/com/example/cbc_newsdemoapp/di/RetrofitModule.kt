package com.example.cbc_newsdemoapp.di

import com.example.cbc_newsdemoapp.BaseApplication
import com.example.cbc_newsdemoapp.network.NewsAPIInterface
import com.example.cbc_newsdemoapp.repository.NewsRepository
import com.example.cbc_newsdemoapp.repository.NewsRepositoryImpl
import com.example.cbc_newsdemoapp.utils.BASE_URL
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitNewsAPIService(retrofit: Retrofit): NewsAPIInterface{
        return retrofit.create(NewsAPIInterface::class.java)
    }
}