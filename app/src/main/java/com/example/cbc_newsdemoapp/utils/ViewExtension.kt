package com.example.cbc_newsdemoapp.utils

import android.view.View

// To set the visibility only based on nullability
fun View.setVisibility(value: Any?) {
    this.visibility =  if(value != null) View.VISIBLE else View.GONE
}

// To set the visibility based on the value of the boolean
fun View.setVisibility(value: Boolean?) {
    this.visibility =  if(value != null && value) View.VISIBLE else View.GONE
}