package com.example.cbc_newsdemoapp.utils

import java.text.SimpleDateFormat
import java.util.*

// Convert Long to Date/Time in required format
fun Long.convertLongToTime(): String {
    val date = Date(this)
    val format = SimpleDateFormat("yyyy.MM.dd", Locale.getDefault())
    return format.format(date)
}
