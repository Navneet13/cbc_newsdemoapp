package com.example.cbc_newsdemoapp.utils

sealed class Outcome<out T, out S> {
    data class Success<out T>(val data: T?): Outcome<T, Nothing>()
    data class Failure<out S>(val errorData: S?): Outcome<Nothing, S>()
    object NetworkLost: Outcome<Nothing, Nothing>()
}