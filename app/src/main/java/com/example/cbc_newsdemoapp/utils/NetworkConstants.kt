package com.example.cbc_newsdemoapp.utils

const val BASE_URL = "https://www.cbc.ca/"
const val API_ENDPOINT = "aggregate_api/v1/items?lineupSlug=news"