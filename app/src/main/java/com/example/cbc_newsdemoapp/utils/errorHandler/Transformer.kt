package com.example.cbc_newsdemoapp.utils.errorHandler

import okhttp3.ResponseBody

interface Transformer<T> {
    fun transform(errorBody: ResponseBody?): T
}