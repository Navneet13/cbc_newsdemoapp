package com.example.cbc_newsdemoapp.utils.errorHandler

import com.example.cbc_newsdemoapp.model.ErrorDataClass
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody

class ErrorDataTransformer : Transformer<ErrorDataClass> {

    override fun transform(errorBody: ResponseBody?): ErrorDataClass {
        val type = object : TypeToken<ErrorDataClass>() {}.type
        return Gson().fromJson(errorBody.toString(), type)
    }
}