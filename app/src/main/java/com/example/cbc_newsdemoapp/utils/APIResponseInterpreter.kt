package com.example.cbc_newsdemoapp.utils

import com.example.cbc_newsdemoapp.utils.errorHandler.Transformer
import retrofit2.Response

suspend fun <T, S> apiCall(
    networkConnectivity: NetworkConnectivityHelper,
    call: suspend () -> Response<T>,
    errorTransformer: Transformer<S>
): Outcome<T, S> {
    if (!networkConnectivity.hasInternetConnection()) {
        return Outcome.NetworkLost
    }
    val response = call.invoke()
    return if (response.isSuccessful) {
        Outcome.Success(response.body())
    } else {
        val errorBody = response.errorBody()
        Outcome.Failure(errorTransformer.transform(errorBody))
    }
}