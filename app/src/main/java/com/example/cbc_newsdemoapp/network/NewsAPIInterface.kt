package com.example.cbc_newsdemoapp.network

import com.example.cbc_newsdemoapp.model.News
import com.example.cbc_newsdemoapp.utils.API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface NewsAPIInterface {

    @GET(API_ENDPOINT)
    suspend fun fetchNewsData(): Response<News>
}