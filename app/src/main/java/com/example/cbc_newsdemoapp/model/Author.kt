package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Author(
    val display: String? = null,
    val image: String? = null,
    val name: String? = null
): Parcelable