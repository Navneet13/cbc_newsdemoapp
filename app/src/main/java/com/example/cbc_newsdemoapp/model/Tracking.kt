package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Tracking(
    val contentarea: String? = null,
    val contenttype: String? = null,
    val subsection1: String? = null,
    val subsection2: String? = null,
    val subsection3: String? = null,
    val subsection4: String? = null
): Parcelable