package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class Metadata(
    val adHierarchy: String? = null,
    val attribution: Attribution ? = null,
    val mpxCategoryName: String? = null,
    val orderLineupId: String? = null,
    val orderLineupSlug: String? = null,
    val ottTitle: @RawValue Any? = null,
    val pageDescription: String? = null,
    val pageTitle: String? = null,
    val polopolyDeptName: String? = null,
    val polopolyExternalId: String? = null,
    val tracking: Tracking ? = null
): Parcelable