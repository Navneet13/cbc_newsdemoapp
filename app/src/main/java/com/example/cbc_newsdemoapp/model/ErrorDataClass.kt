package com.example.cbc_newsdemoapp.model

data class ErrorDataClass(val message: String, val code: Int)