package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Flags(
    val label: String? = null,
    val status: String? = null
): Parcelable