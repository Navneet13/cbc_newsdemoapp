package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Headline(
    val mediaId: String? = null,
    val type: String? = null
): Parcelable