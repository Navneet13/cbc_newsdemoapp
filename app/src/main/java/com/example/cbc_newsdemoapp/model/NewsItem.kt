package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class NewsItem(
    val active: Boolean,
    val description: String,
    val embedTypes: String? = null,
    val id: Int,
    val images: Images? = null,
    val language: String? = null,
    val publishedAt: Long,
    val readablePublishedAt: String? = null,
    val readableUpdatedAt: String? = null,
    val source: String? = null,
    val sourceId: String? = null,
    val title: String,
    val type: String,
    val typeAttributes: TypeAttributes? = null,
    val updatedAt: Long? = null,
    val version: String? = null
): Parcelable