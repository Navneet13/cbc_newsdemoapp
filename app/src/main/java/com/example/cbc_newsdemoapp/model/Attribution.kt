package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Attribution(
    val level1: String? = null,
    val level2: String? = null,
    val level3: String? = null
): Parcelable