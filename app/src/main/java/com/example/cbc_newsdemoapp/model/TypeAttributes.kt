package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class TypeAttributes(
    val author: Author? = null,
    val body: Body? = null,
    val categories: List<Category> ? = null,
    val commentsSectionId: String? = null,
    val contextualHeadlines: @RawValue List<Any>? = null,
    val deck: String? = null,
    val displayComments: Boolean? = null,
    val flag: String? = null,
    val flags: Flags? = null,
    val headline: Headline? = null,
    val imageAspects: String? = null,
    val imageLarge: String? = null,
    val imageSmall: String? = null,
    val mediaCaptionUrl: @RawValue Any? = null,
    val mediaDuration: @RawValue Any? = null,
    val mediaId: @RawValue Any? = null,
    val mediaStreamType: @RawValue Any? = null,
    val sectionLabels: List<String>? = null,
    val sectionList: List<String>? = null,
    val show: String? = null,
    val showSlug: String? = null,
    val trending: Trending? = null,
    val url: String? = null,
    val urlSlug: String? = null
): Parcelable