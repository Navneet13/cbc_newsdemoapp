package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Trending(
    val numViewers: Int? = null,
    val numViewersSRS: @RawValue Any? = null
): Parcelable