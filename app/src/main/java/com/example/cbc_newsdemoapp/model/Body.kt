package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Body(
    val containsAudio: Boolean? = null,
    val containsPhotogallery: Boolean? = null,
    val containsVideo: Boolean? = null,
    val formatVersion: Int? = null
): Parcelable