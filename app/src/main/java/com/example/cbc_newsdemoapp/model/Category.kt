package com.example.cbc_newsdemoapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Category(
    val bannerImage: String? = null,
    val id: Int? = null,
    val image: String? = null,
    val metadata: Metadata ? = null,
    val name: String? = null,
    val path: String? = null,
    val priority: Int? = null,
    val priorityWhenInlined: Int? = null,
    val slug: String? = null,
    val type: String? = null
): Parcelable