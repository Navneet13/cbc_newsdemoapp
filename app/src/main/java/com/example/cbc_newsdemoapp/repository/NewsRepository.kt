package com.example.cbc_newsdemoapp.repository

import com.example.cbc_newsdemoapp.model.ErrorDataClass
import com.example.cbc_newsdemoapp.model.News
import com.example.cbc_newsdemoapp.utils.Outcome

interface NewsRepository {
    suspend fun fetchNewsDetails(): Outcome<News, ErrorDataClass>
}