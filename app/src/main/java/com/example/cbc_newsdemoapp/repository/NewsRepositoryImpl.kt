package com.example.cbc_newsdemoapp.repository

import com.example.cbc_newsdemoapp.model.ErrorDataClass
import com.example.cbc_newsdemoapp.model.News
import com.example.cbc_newsdemoapp.network.NewsAPIInterface
import com.example.cbc_newsdemoapp.utils.errorHandler.ErrorDataTransformer
import com.example.cbc_newsdemoapp.utils.NetworkConnectivityHelper
import com.example.cbc_newsdemoapp.utils.Outcome
import com.example.cbc_newsdemoapp.utils.apiCall
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(private val newsAPIInterface: NewsAPIInterface) :
    NewsRepository {

    @Inject
    lateinit var networkConnectivity: NetworkConnectivityHelper

    override suspend fun fetchNewsDetails(): Outcome<News, ErrorDataClass> {
        return apiCall(
            networkConnectivity,
            call = { newsAPIInterface.fetchNewsData() },
            errorTransformer = ErrorDataTransformer()
        )
    }
}