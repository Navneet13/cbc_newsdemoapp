package com.example.cbc_newsdemoapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.cbc_newsdemoapp.databinding.FragmentNewsDescriptionBinding
import com.example.cbc_newsdemoapp.model.NewsItem
import com.example.cbc_newsdemoapp.utils.convertLongToTime
import com.example.cbc_newsdemoapp.utils.setVisibility

class NewsDescriptionsFragment : Fragment() {

    // Reference for View Binding
    private lateinit var fragmentBinding: FragmentNewsDescriptionBinding

    // Reference for extracting the arguments
    private val args: NewsDescriptionsFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        fragmentBinding = FragmentNewsDescriptionBinding.inflate(inflater, container, false)
        return fragmentBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val newsItem = args.newsItemActions
        // Fill up the views with the data
        setUpViews(newsItem)
    }

    // Fill the views with respective data
    private fun setUpViews(newsItem: NewsItem) {
        fragmentBinding.apply {
            tvNewsHeadline.text = newsItem.title
            tvDatePublished.text = newsItem.publishedAt.convertLongToTime()
            ivNews.load(newsItem.typeAttributes?.imageLarge)
            tvNewsDescription.text = newsItem.description
            val authorName = newsItem.typeAttributes?.author?.name
            if (!authorName.isNullOrEmpty()) tvAuthor.text = authorName else tvAuthor.setVisibility(
                false
            )
        }
    }
}