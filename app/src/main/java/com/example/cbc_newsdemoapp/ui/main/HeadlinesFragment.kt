package com.example.cbc_newsdemoapp.ui.main

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cbc_newsdemoapp.R
import com.example.cbc_newsdemoapp.databinding.FragmentHeadlinesBinding
import com.example.cbc_newsdemoapp.databinding.NetworkLostDialogBinding
import com.example.cbc_newsdemoapp.model.NewsItem
import com.example.cbc_newsdemoapp.ui.viewmodel.NewsViewModel
import com.example.cbc_newsdemoapp.utils.UIUtils
import com.example.cbc_newsdemoapp.utils.setVisibility
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeadlinesFragment : Fragment() {

    // Reference for View Binding
    private lateinit var fragmentBinding: FragmentHeadlinesBinding

    // Initiating ViewModel
    private val newsViewModel: NewsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentHeadlinesBinding.inflate(inflater, container, false)
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerView(view.context, emptyList())
        newsViewModel.fetchNewsInfo()
        bindViewModel(view.context)
        setUpClickListeners()
    }

    // Set the recycler view for News Headlines
    private fun setUpRecyclerView(context: Context, list: List<NewsItem>) {
        fragmentBinding.recycleNewsHeadlines.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = HeadlinesRVAdapter(list) { view, newsItem ->
                val action = HeadlinesFragmentDirections.actionToNewsDescriptionFragment(newsItem)
                view.findNavController().navigate(action)
            }
        }
    }

    // Binding the views with data from observables
    private fun bindViewModel(context: Context) {
        // Observing success response from API
        newsViewModel.newsItemsList.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                (fragmentBinding.recycleNewsHeadlines.adapter as HeadlinesRVAdapter).setItems(it)
                fragmentBinding.ivFilterType.setVisibility(true)
            }
        }

        // Observing Error from the API
        newsViewModel.newsInfoError.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        }

        // Observing network connectivity
        newsViewModel.networkAvailable.observe(viewLifecycleOwner) {
            if (!it) {
                showNetworkLostAlertDialog(context)
            }
        }

        // Observing loading state
        newsViewModel.isLoading.observe(viewLifecycleOwner) {
            fragmentBinding.pbLoading.setVisibility(it)
        }
    }

    // Click Listeners for all Views
    private fun setUpClickListeners() {
        fragmentBinding.apply {
            ivFilterType.setOnClickListener {
                filterNewsTypesPopUp(it)
            }

            ivRefresh.setOnClickListener {
                newsViewModel.fetchNewsInfo()
            }
        }
    }

    // Show the alert dialog with appropriate options to select on network unavailability
    private fun showNetworkLostAlertDialog(context: Context) {
        val builder = AlertDialog.Builder(context)

        val networkLostDialogBinding =
            NetworkLostDialogBinding.inflate(LayoutInflater.from(context))
        builder.setView(networkLostDialogBinding.root)

        val dialog = builder.show()

        networkLostDialogBinding.apply {
            // Provides chance to retry and call api again
            btnRetry.setOnClickListener {
                dialog.dismiss()
                newsViewModel.fetchNewsInfo()
            }

            // Dismiss the dialog box
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    // Displays the Pop up with list of News Types to filter the news from
    private fun filterNewsTypesPopUp(view: View) {
        val listPopupWindow = UIUtils.getPlainListPopUpWindow(
            context = requireContext(),
            items = newsViewModel.getNewsTypeList(newsViewModel.newsItemList),
            anchor = view,
            cellLayoutRes = R.layout.news_type_popup
        )

        listPopupWindow.setOnItemClickListener { _, _, index, _ ->
            listPopupWindow.dismiss()
            displayFilteredList(index)
        }

        listPopupWindow.show()
    }

    // Displays the filtered news acc. to Type in the recycler view
    private fun displayFilteredList(selectedIndex: Int) {
        val newsFullList = newsViewModel.newsItemList
        val newsTypesList = newsViewModel.getNewsTypeList(newsFullList)
        val selectedType = newsTypesList[selectedIndex]
        val listToBeDisplayed = newsFullList.filter {
            it.type == selectedType
        }
        (fragmentBinding.recycleNewsHeadlines.adapter as HeadlinesRVAdapter).setItems(
            listToBeDisplayed
        )
    }
}