package com.example.cbc_newsdemoapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cbc_newsdemoapp.databinding.ItemNewsHeadlineBinding
import com.example.cbc_newsdemoapp.model.NewsItem
import com.example.cbc_newsdemoapp.utils.convertLongToTime

class HeadlinesRVAdapter(
    newsList: List<NewsItem>,
    private val onItemClickListener: (View, newsItem: NewsItem) -> Unit
) : RecyclerView.Adapter<HeadlinesRVAdapter.HeadlineViewHolder>() {

    private val mutableNewsList = newsList.toMutableList()

    // Inside the onCreateViewHolder inflated the view of ItemNewsHeadlineBinding
    // and returned new ViewHolder object containing this layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadlineViewHolder {
        val binding =
            ItemNewsHeadlineBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HeadlineViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HeadlineViewHolder, position: Int) {
        val news = mutableNewsList[position]
        holder.bind(news)
        holder.itemView.setOnClickListener {
            onItemClickListener(it, news)
        }
    }

    override fun getItemCount() = mutableNewsList.size

    fun setItems(newList: List<NewsItem>) {
        mutableNewsList.clear()
        mutableNewsList.addAll(newList)
        notifyDataSetChanged()
    }

    inner class HeadlineViewHolder(private val binding: ItemNewsHeadlineBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(news: NewsItem) {
            binding.apply {
                ivNews.load(news.typeAttributes?.imageLarge)
                tvNewsHeadline.text = news.title
                datePublished.text = news.publishedAt.convertLongToTime()
            }
        }
    }
}