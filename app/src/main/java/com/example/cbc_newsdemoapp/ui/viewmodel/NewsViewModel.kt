package com.example.cbc_newsdemoapp.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cbc_newsdemoapp.model.NewsItem
import com.example.cbc_newsdemoapp.repository.NewsRepository
import com.example.cbc_newsdemoapp.utils.Outcome
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(private val newsRepository: NewsRepository) : ViewModel() {

    private var _newsItemsList = MutableLiveData<List<NewsItem>>()
    val newsItemsList: LiveData<List<NewsItem>> = _newsItemsList

    private var _newsInfoError = MutableLiveData<String>()
    val newsInfoError: LiveData<String> = _newsInfoError

    private var _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private var _networkAvailable = MutableLiveData<Boolean>()
    val networkAvailable: LiveData<Boolean> = _networkAvailable

    lateinit var newsItemList: List<NewsItem>

    // Fetch the detailed information about the News
    fun fetchNewsInfo() {
        _isLoading.value = true
        _networkAvailable.value = true
        viewModelScope.launch {
            when (val response = newsRepository.fetchNewsDetails()) {
                is Outcome.Failure -> {
                    _isLoading.value = false
                    _newsInfoError.value = response.errorData?.message
                }
                Outcome.NetworkLost -> {
                    _isLoading.value = false
                    _networkAvailable.value = false
                }
                is Outcome.Success -> {
                    val newsData = response.data ?: emptyList()
                    _isLoading.value = false
                    _newsItemsList.value = newsData
                    newsItemList = newsData
                }
            }
        }
    }

    // Get the list of all News' Types
    fun getNewsTypeList(newsItemList: List<NewsItem>): List<String> {
        lateinit var typesList: List<String>
        if (newsItemList.isNotEmpty()) {
            typesList = newsItemList.map{
                it.type
            }.toSet().toList()
        }
        return typesList
    }
}