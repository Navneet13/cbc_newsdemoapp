package com.example.cbc_newsdemoapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.cbc_newsdemoapp.model.NewsItem
import com.example.cbc_newsdemoapp.repository.NewsRepository
import com.example.cbc_newsdemoapp.ui.viewmodel.NewsViewModel
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsUnitTests {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var newsViewModel: NewsViewModel

    private val newsRepository = Mockito.mock(NewsRepository::class.java)

    @Before
    fun before() {
        newsViewModel = NewsViewModel(newsRepository)
    }

    @Test
    fun testGetNewsTypeList() {
        val newsItemList = mutableListOf<NewsItem>(
            NewsItem(title = "Test Title1", description = "Test Description", publishedAt = 328327867, id = 12, active = true, type = "Story"),
            NewsItem(title = "Test Title2", description = "Test Description2", publishedAt = 328329085, id = 11, active = true, type = "Content Package"),
            NewsItem(title = "Test Title3", description = "Test Description2", publishedAt = 328329090, id = 10, active = true, type = "Content Package"),
        )

        val expectedResult =  listOf("Story", "Content Package")
        val actualResult = newsViewModel.getNewsTypeList(newsItemList)

        assertEquals("getNewsTypeList() Result: ", expectedResult , actualResult)
    }
}